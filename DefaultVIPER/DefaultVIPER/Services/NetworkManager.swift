//
//  NetworkManager.swift
//  DefaultVIPER
//
//  Created by Sergey Stelmakh on 03.11.2021.
//

import Foundation

class NetworkManager {
    
    static let shared = NetworkManager()
    
    private let api =  "https://jsonplaceholder.typicode.com/posts"
    
    private init() {}
    
    func fetch(completion: @escaping (_ posts: [Posts]) -> Void) {
        guard let url = URL(string: api) else { return }
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else {
                print(error?.localizedDescription ?? "No Discription")
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let posts = try decoder.decode([Posts].self, from: data)
                DispatchQueue.main.async {
                    completion(posts)
                }
            } catch let error {
                print("Error json", error)
            }
        }.resume()
    }
}
