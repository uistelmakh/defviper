//
//  Posts.swift
//  DefaultVIPER
//
//  Created by Sergey Stelmakh on 03.11.2021.
//

import Foundation

struct Posts: Decodable {
    var userId: Int
    var id: Int
    var title: String
    var body: String
}
