//
//  PostsDetailsInteractor.swift
//  DefaultVIPER
//
//  Created by Sergey Stelmakh on 03.11.2021.
//

import Foundation

/// Протокол для работы с PostsDetailsInteractor
protocol PostsDetailsBusinessLogic {
    var presenter: PostsDetailsPresentationLogic? { get set }
    var posts: Posts? { get set }
    
    /// Получение постов из переданных данных
    func getFullInfoPosts()
}

/// Interactor VIPER-модуля Постов
class PostsDetailsInteractor: PostsDetailsBusinessLogic {
    weak var presenter: PostsDetailsPresentationLogic?
    var posts: Posts?
    
    func getFullInfoPosts() {
        self.presenter?.getInfoPostsFromURL(posts: self.posts!)
    }
}

