//
//  PostsDetailsConfigurator.swift
//  DefaultVIPER
//
//  Created by Sergey Stelmakh on 03.11.2021.
//

import Foundation
import UIKit
 
/// Конфигуратор VIPER-модуля PostsDetailsViewController
class PostsDetailsConfigurator {
    static func createModule(with posts: Posts) -> UIViewController {
        let viewController = PostsDetailsViewController()
        let presenter: PostsDetailsViewOutput & PostsDetailsPresentationLogic  = PostsDetailsPresenter()
        
        viewController.presenter = presenter
        viewController.presenter?.view = viewController
        viewController.presenter?.interactor = PostsDetailsInteractor()
        viewController.presenter?.interactor?.posts = posts
        viewController.presenter?.interactor?.presenter = presenter

        return viewController
    }
}
