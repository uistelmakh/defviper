//
//  PostsDetailsPresenter.swift
//  DefaultVIPER
//
//  Created by Sergey Stelmakh on 03.11.2021.
//

import Foundation

/// Протокол для работы Presenter с PostsDetailsViewController
protocol PostsDetailsViewOutput {
    var view: PostsDetailsProtocol? { get set }
    var interactor: PostsDetailsBusinessLogic? { get set }
    
    /// загрузка презентера с данными
    func viewDidLoad()
}

/// Логика презентации
protocol PostsDetailsPresentationLogic: AnyObject {
    /// Подготовка данных в правильном формате и отправка на вью
    func getInfoPostsFromURL(posts: Posts)
}

/// Презентер VIPER-модуля постов
class PostsDetailsPresenter: PostsDetailsViewOutput {
    
    weak var view: PostsDetailsProtocol?
    var interactor: PostsDetailsBusinessLogic?
    
    func viewDidLoad() {
        interactor?.getFullInfoPosts()
    }
}

// MARK: - PostsDetailsPresentationLogic
extension PostsDetailsPresenter: PostsDetailsPresentationLogic {
    func getInfoPostsFromURL(posts: Posts) {
        view?.onGetInfoPostsFromURL("userId - \(posts.userId), id - \(posts.id)", title: posts.title)
    }
}
