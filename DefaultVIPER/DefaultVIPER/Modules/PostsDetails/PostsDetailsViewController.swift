//
//  PostsDetailsViewController.swift
//  DefaultVIPER
//
//  Created by Sergey Stelmakh on 03.11.2021.
//

import UIKit

/// Протокол отображения PostsDetailsViewController-а
protocol PostsDetailsProtocol: AnyObject {
    func onGetInfoPostsFromURL(_ userID: String, title: String)
}

/// Экран отображения делальной информации поста
class PostsDetailsViewController: UIViewController {
    
    /// Ссылка на слой презентации
    var presenter: PostsDetailsViewOutput?
    
    // MARK: - UI
    var userIdLabel = UILabel()
    var titleLabel = UILabel()
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
        setup()
    }
}

// MARK: - Setup
extension PostsDetailsViewController {
    func setup() {
        view.backgroundColor = .white
        self.userIdLabel.translatesAutoresizingMaskIntoConstraints = false
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(userIdLabel)
        view.addSubview(titleLabel)
        autoLayout()
    }
    
    func autoLayout() {
        userIdLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 100).isActive = true
        userIdLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 150).isActive = true
        
        titleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 100).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -50).isActive = true
        titleLabel.topAnchor.constraint(equalTo: userIdLabel.bottomAnchor, constant: 200).isActive = true
    }
}

// MARK: - PostsDetailsProtocol
extension PostsDetailsViewController: PostsDetailsProtocol {
    func onGetInfoPostsFromURL(_ userID: String, title: String) {
        self.userIdLabel.text = userID
        self.titleLabel.text = title
        self.titleLabel.numberOfLines = 0
        self.navigationItem.title = userID
    }
}
