//
//  PostsPresenter.swift
//  DefaultVIPER
//
//  Created by Sergey Stelmakh on 03.11.2021.
//

import Foundation

/// Протокол для работы Presenter с PostsViewController
protocol PostsViewOutput: AnyObject {
    
    var view: PostsProtocol? { get set }
    var interactor: PostsBusinessLogic? { get set }
    var router: PostsRoutingLogic? { get set }
    
    var postsStrings: [String]? { get set }
    
    /// загрузка презентера с данными
    func viewDidLoad()
    
    /// кол-во строк в таблице
    func numberOfRowsInSection() -> Int
    
    /// текст в ячейке таблицы
    func textLabelText(indexPath: IndexPath) -> String?
    
    func didSelectRowAt(index: Int)
    func deselectRowAt(index: Int)
}

/// Логика презентации
protocol PostsPresentationLogic: AnyObject {
    /// Получение постов и передача на вью
    func fetchPostsSuccess(posts: [Posts])
    
    /// Передача постов
    func getPostsSuccess(_ posts: Posts)
}

/// Презентер VIPER-модуля постов
class PostsPresenter: PostsViewOutput {
    weak var view: PostsProtocol?
    var interactor: PostsBusinessLogic?
    var router: PostsRoutingLogic?
    var configurator: PostsConfiguratorProtocol?
    
    var postsStrings: [String]?
    
    func viewDidLoad() {
        interactor?.loadPosts()
    }
    
    func numberOfRowsInSection() -> Int {
        guard let postStrings = self.postsStrings else { return 0 }
        
        print(postStrings)
        
        return postStrings.count
    }
    
    func textLabelText(indexPath: IndexPath) -> String? {
        guard let postsStrings = self.postsStrings else {
            return nil
        }
        
        return postsStrings[indexPath.row]
    }
    
    func didSelectRowAt(index: Int) {
        self.interactor?.retrievePosts(at: index)
    }
    
    func deselectRowAt(index: Int) {
        view?.deselectRowAt(row: index)
    }
}

// MARK: - PostsPresentationLogic
extension PostsPresenter: PostsPresentationLogic {
    func fetchPostsSuccess(posts: [Posts]) {
        self.postsStrings = posts.compactMap { $0.title }
        view?.onFetchPostsSuccess()
    }
    
    func getPostsSuccess(_ posts: Posts) {
        router?.pushToQuoteDetail(on: view!, with: posts)
    }
}
