//
//  PostsConfigurator.swift
//  DefaultVIPER
//
//  Created by Sergey Stelmakh on 03.11.2021.
//

import Foundation
import UIKit

protocol PostsConfiguratorProtocol {
    /// сборка модуля
    static func createModule() -> UINavigationController
}

/// Конфигуратор VIPER-модуля PostsViewController
class PostsConfigurator: PostsConfiguratorProtocol {
    static func createModule() -> UINavigationController {
        let viewController = PostsViewController()
        let navigationController = UINavigationController(rootViewController: viewController)
        
        let presenter: PostsViewOutput & PostsPresentationLogic  = PostsPresenter()
        
        viewController.presenter = presenter
        viewController.presenter?.router = PostsRouter()
        viewController.presenter?.view = viewController
        viewController.presenter?.interactor = PostsInteractor()
        viewController.presenter?.interactor?.presenter = presenter

        return navigationController
    }
}
