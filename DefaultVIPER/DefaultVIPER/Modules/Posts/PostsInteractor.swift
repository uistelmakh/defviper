//
//  PostsInteractor.swift
//  DefaultVIPER
//
//  Created by Sergey Stelmakh on 03.11.2021.
//

import Foundation

/// Протокол для работы с PostsInteractor
protocol PostsBusinessLogic: AnyObject {
    var presenter: PostsPresentationLogic? { get set }
    
    /// Получение данных из NetworkManager
    func loadPosts()
    
    /// Получение поста по индексу
    func retrievePosts(at index: Int)
}

/// Interactor VIPER-модуля Постов
class PostsInteractor: PostsBusinessLogic {
    weak var presenter: PostsPresentationLogic?
    
    var posts: [Posts]?
    
    func loadPosts() {
        NetworkManager.shared.fetch { posts in
            self.posts = posts
            self.presenter?.fetchPostsSuccess(posts: posts)
        }
    }
    
    func retrievePosts(at index: Int) {
        self.presenter?.getPostsSuccess(self.posts![index])
    }
}

