//
//  PostsRouter.swift
//  DefaultVIPER
//
//  Created by Sergey Stelmakh on 03.11.2021.
//

import Foundation
import UIKit

/// Протокол логики роутера PostsRouter
protocol PostsRoutingLogic {
    /// Переход на новый контроллер и передача постов
    func pushToQuoteDetail(on view: PostsProtocol, with posts: Posts)
}

class PostsRouter: PostsRoutingLogic {
    func pushToQuoteDetail(on view: PostsProtocol, with posts: Posts) {
        
        let postsDetailsViewController = PostsDetailsConfigurator.createModule(with: posts)
        
        guard let viewController = view as? PostsViewController else { return }
        viewController.navigationController?.pushViewController(postsDetailsViewController, animated: true)
    }
}


