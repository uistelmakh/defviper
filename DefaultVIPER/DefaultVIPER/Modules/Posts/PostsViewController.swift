//
//  PostsViewController.swift
//  DefaultVIPER
//
//  Created by Sergey Stelmakh on 03.11.2021.
//

import UIKit

/// Протокол отображения PostsViewController-а
protocol PostsProtocol: AnyObject {
    /// Получение ответа от presentera'а и обновление таблицы
    func onFetchPostsSuccess()
    func deselectRowAt(row: Int)
}

/// Экран отображения постов
class PostsViewController: UIViewController {
    
    /// Ссылка на слой презентации
    var presenter: PostsViewOutput?
    
    // MARK: - UI
    var tableView = UITableView()

    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.presenter?.viewDidLoad()
        setup()
    }
    
    override func viewDidLayoutSubviews() {
        self.tableView.frame = view.bounds
    }
}

// MARK: - Setup
extension PostsViewController {
    func setup() {
        self.view.addSubview(self.tableView)
        self.tableView.rowHeight = 70
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.navigationItem.title = "JSONPlaceholder"
    }
}

// MARK: - UITableViewDataSource
extension PostsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.numberOfRowsInSection() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = presenter?.textLabelText(indexPath: indexPath)
        return cell
    }
}

// MARK: - UITableViewDelegate
extension PostsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.didSelectRowAt(index: indexPath.row)
        presenter?.deselectRowAt(index: indexPath.row)
    }
}

// MARK: - PostsProtocol
extension PostsViewController: PostsProtocol {
    func onFetchPostsSuccess() {
        self.tableView.reloadData()
    }
    
    func deselectRowAt(row: Int) {
        self.tableView.deselectRow(at: IndexPath(row: row, section: 0), animated: true)
    }
}
